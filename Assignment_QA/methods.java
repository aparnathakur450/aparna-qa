package javaconcepts;

public class methods {
	
	int x=100;
	int y=90;

	public static void main(String[] args) 
	{
		System.out.println("The program execution has started");
		methods meth= new methods();
		meth.sum();
		System.out.println("The value of X is:"+meth.x);
		System.out.println("The value of Y is:"+meth.y);
		System.out.println("The program execution has ended");	
	}
	 
	public void sum()
	{
		int a=90;
		int b=76;
		int c=a+b;
		System.out.println("The sum of two number is:"+c);
	}

}
