package inheritance;

public class ChildClass extends BaseClass
{

	public static void main(String[] args)
	{
		ChildClass objc=new ChildClass();
		objc.add();
		objc.div();
		objc.sub();
		objc.mul();
	}
	public void mul()
	{
		System.out.println("Multipication method from Child Class");
	}
	public void div()
	{
		System.out.println("Division method from Child Class");
	}
}
